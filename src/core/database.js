const bookshelf = require('bookshelf')
const knex      = require('knex')
const config    = require('../../configs')
const db        = bookshelf(knex(config.database));
module.exports  = { db }