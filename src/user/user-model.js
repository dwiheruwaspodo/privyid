const core            = require('../core')
const { uploadModel } = require('../upload/upload-model')

const bookshelf = core.database.db;

const userModel = bookshelf.Model.extend({
	tableName     :'users',
	idAttribute   :'id',
	hasTimestamps : true,
	files       : function() {
        return this.hasMany(uploadModel, 'id_user')
    }
});

module.exports = { userModel }