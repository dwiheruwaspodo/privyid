const express       = require('express')
const core          = require('../core')
const bcrypt        = require('bcryptjs')
const jwt           = require('jsonwebtoken')
const config        = require('../../configs')
const { userModel } = require('./user-model')

const { validateRegister, validateLogin } = require('./user-validator')
const { saltRounds, jwtToken }            = config.jwt

const register = async (req, res) => {	
	let post       = req.body;
	let validation = await validateRegister(post)

	if (validation.fails()) {
		return res.json({
			status  : "fail",
			data : validation.errors.all()
		})
	}

	let check = await checkUserByMail(post.email)

	if (check) {
		return res.status(200).json({
			status: "fail", 
			data: { message: "email already register" }
		})
	}

	let data = {
		'username'     : post.username,
		'email'        : post.email,
		'password'     : bcrypt.hashSync(post.password, saltRounds),
	}

	let save = await userModel
	.forge().save(data)
	.then(function (user) {
    	return res.status(200).json({
    		status: "success", 
    		data: { user: user }
    	})
    })
    .catch(function (err) {
    	return res.status(500).json({
    		status: "fail",
    		data: { message: err.message } 
    	})
    });
}

const checkUserByMail = (mail) => {
	return userModel.forge().where({email: mail}).fetch()
}

const login = async (req, res) => {	
	let post       = req.body;
	let validation = await validateLogin(post)

	if (validation.fails()) {
		return res.json({
			status  : "fail",
			data : validation.errors.all()
		})
	}

	let check = await checkUserByMail(post.email)

	if (check) {
		check = check.toJSON()
		if (bcrypt.compareSync(post.password, check.password)) {
			let token = {
				status: "success",
				data : { token  : jwt.sign(check, jwtToken, { expiresIn: 60 * 60}) }
			}

			return res.json(token)
		}
	}

	return res.status(200).json({
		status  : "fail",
		data : { message : "user not found" } 
	})
}

const mydata = async (req, res) => {
	let me = await userModel
				.forge()
				.where({id: req.user.id})
				.fetch({withRelated: ['files']})
	if (me) {
		me = me.toJSON()
		return res.status(200).json({
			status: "success", 
    		data: { user: me }
		})
	}

	return res.status(500).json({
		status: "fail",
		data : { message : "user not found" } 
	})
}

module.exports = { register, login, mydata }