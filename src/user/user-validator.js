const Validator = require('validatorjs')

const validateRegister = async (data) => {
	let rules = {
		email    : 'required|email',
		password : 'required',
		username : 'required',
	};

	return new Validator(data, rules);
}

const validateLogin = async (data) => {
	let rules = {
		email    : 'required|email',
		password : 'required',
	};

	return new Validator(data, rules);
}

module.exports = { validateRegister, validateLogin }