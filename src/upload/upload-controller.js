const express         = require('express')
const core            = require('../core')
const bcrypt          = require('bcryptjs')
const jwt             = require('jsonwebtoken')
const config          = require('../../configs')
const { uploadModel } = require('./upload-model')
const multer          = require('multer')
const uploadDir       = multer({ dest: 'files/' })
const path            = require("path")

const storage = multer.diskStorage({
	destination : __dirname + '/../../files/',
	filename    : function (req, file, cb) {
  		cb(null, file.originalname)
  	}
})

const uploadStorage = multer({ storage: storage })
const uploadFile    = uploadStorage.single('avatar')

const upload = async (req, res) => {

	if (req.file === undefined) {
		return res.json({
			status  : "fail",
			data : {
	      		avatar: [
	      			"The avatar field is required."
	      		]
	      	}
		})
	}

	uploadFile(req, res, function (err) {
	    if (err instanceof multer.MulterError) {
	      return res.status(500).json({
	      	status: "fail", 
	      	data: { message: err }
	      })
	    } else if (err) {
	      	return res.status(500).json({
	      		status: "fail", 
	      		data: { message: err }
	      	})
	    }
	})

	let dataUpload = {
		path    : req.file.path,
		file    : req.file.filename,
		id_user : req.user.id
	}

	let save = await uploadModel
	.forge().save(dataUpload)
	.then(function (uploadresult) {
    	return res.status(200).json({
    		status: "success", 
    		data: { file: uploadresult }
    	})
    })
    .catch(function (err) {
    	return res.status(500).json({
    		status: "fail", 
    		data: { message: err.message }
    	})
    });
}

module.exports = { upload, uploadFile }