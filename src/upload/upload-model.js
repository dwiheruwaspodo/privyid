const core            = require('../core')
const { userModel } = require('../user/user-model')

const bookshelf = core.database.db;

const uploadModel = bookshelf.Model.extend({
	tableName     :'user_uploads',
	idAttribute   :'id',
	hasTimestamps : true,
 	user       : function() {
         return this.belongsTo(userModel, 'id')
     }
});

module.exports = { uploadModel }