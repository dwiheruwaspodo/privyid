const express = require('express')
const routes  = express.Router();

const { auth } = require('./middleware')

const { register, login, mydata } = require('./user/user-controller')
routes.post('/register', register);
routes.post('/login', login);
routes.get('/me', auth(), mydata);

const { upload, uploadFile } = require('./upload/upload-controller')
routes.post('/upload', uploadFile, auth(), upload);

module.exports = { routes }