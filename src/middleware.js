const jwt          = require('jsonwebtoken')
const config       = require('../configs')
const { jwtToken } = config.jwt

const auth = () => {
	return async (req, res, next) => {
		let token = req.headers

		try {
			var decoded = jwt.verify(token.authorization, jwtToken);
			req.user = decoded
			return next()
		} catch(err) {
		  	return res.json({error: "Invalid token"})
		}
	}
}

module.exports = { auth }