'use strict';

require('custom-env').env()

module.exports = {
	port: process.env.PORT,
	database : {
		client: 'mysql',
		connection: {
			host    : process.env.DB_HOST,
			user    : process.env.DB_USER,
			password: process.env.DB_PASS,
			database: process.env.DB_NAME,
			charset : 'utf8'
		},
	  	migrations: {
	    	tableName: 'knex_migrations'
	  	}
	},
	jwt : {
		saltRounds: 10,
		jwtToken: "token"
	}
}