const express    = require('express')
const bodyParser = require('body-parser')
const app        = express()
const config     = require('./configs')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// routes
const { routes } = require('./src/routes')
app.use('/api', routes)

app.listen(config.port, function() {
	console.log('listening on port' + config.port)
});