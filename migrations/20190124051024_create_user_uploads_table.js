
exports.up = function(knex, Promise) {
    return knex.schema.createTableIfNotExists('user_uploads', table => {
  		table.increments().primary()
  		table.integer('id_user').unsigned().references('id').inTable('users').onDelete('set null')
  		table.string('path', 100).notNullable()
  		table.string('file', 100).notNullable()
  		table.timestamps()
  	});
};

exports.down = function(knex, Promise) {
  	return knex.schema.dropTable('user_uploads');
};
